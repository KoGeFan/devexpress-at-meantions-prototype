﻿using System;
using DevExpress.XtraRichEdit.API.Native;

namespace CommentMentionsTest
{
    public class LastWordSearchResult
    {
        public string LastWord { get; set; }
        public string SymbolBeforeLastWord { get; set; }
        public DocumentRange Range { get; set; }
    }
}