﻿using DevExpress.XtraBars;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views.Grid;

namespace CommentMentionsTest
{
    public class CommentEditor : UserControl
    {
        private List<string> Users =
            new List<string>
            {
                "pm",
                "dh",
                "al",
                "vm",
                "daria"
            };

        private DevExpress.XtraRichEdit.RichEditControl textEditor;
        private PopupControlContainer userPopupContainer;
        private System.ComponentModel.IContainer components;
        private GridControl userGrid;
        private GridView userView;
        
        private BarManager _manager;

        public CommentEditor()
        {
            components = new System.ComponentModel.Container();
            textEditor = new DevExpress.XtraRichEdit.RichEditControl();
            userPopupContainer = new PopupControlContainer(components);
            ((System.ComponentModel.ISupportInitialize)userPopupContainer).BeginInit();
            SuspendLayout();
            // 
            // richEditControl2
            // 
            textEditor.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Draft;
            textEditor.Dock = DockStyle.Fill;
            textEditor.LayoutUnit = DevExpress.XtraRichEdit.DocumentLayoutUnit.Pixel;
            textEditor.Location = new Point(0, 0);
            textEditor.Name = "textEditor";
            textEditor.Options.HorizontalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            textEditor.Options.VerticalRuler.Visibility = DevExpress.XtraRichEdit.RichEditRulerVisibility.Hidden;
            textEditor.Size = new Size(771, 502);
            textEditor.TabIndex = 1;
            textEditor.AutoCorrect += textEditor_AutoCorrect;
            textEditor.KeyDown += OnKeyDown;
            textEditor.TextChanged += TextEditorOnTextChanged;
            // 
            // popupControlContainer1
            // 
            userPopupContainer.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            userPopupContainer.Location = new System.Drawing.Point(0, 0);
            userPopupContainer.Name = "popupControlContainer1";
            userPopupContainer.Size = new System.Drawing.Size(250, 289);
            userPopupContainer.TabIndex = 2;
            userPopupContainer.Visible = false;
            // 
            // CommentEditor
            // 
            AutoScaleDimensions = new SizeF(9F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(userPopupContainer);
            Controls.Add(textEditor);
            Name = "CommentEditor";
            Size = new Size(771, 502);
            ((System.ComponentModel.ISupportInitialize)userPopupContainer).EndInit();
            ResumeLayout(false);
            
            Load += OnLoad;
        }

        private void TextEditorOnTextChanged(object sender, EventArgs e)
        {
            if (!IsPopUpShown) 
                return;
            
            var caretPos = textEditor.Document.CaretPosition.ToInt();
            if (caretPos == 0)
            {
                userPopupContainer.HidePopup();
                return;
            }
                
            var previousSymbol = textEditor.Document.GetText(textEditor.Document.CreateRange(caretPos - 1, 1));
            if (previousSymbol == "@")
            {
                userView.FindFilterText = string.Empty;
                return;
            }
                
            var searchResult = GetLastWord(caretPos);
            if (searchResult.SymbolBeforeLastWord == "@" && !string.IsNullOrWhiteSpace(searchResult.LastWord))
            {
                userView.FindFilterText = searchResult.LastWord;
                return;
            }
                
            userPopupContainer.HidePopup();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            userGrid = new GridControl();
            userGrid.Parent = this;
            userGrid.Dock = DockStyle.Fill;
            userGrid.DataSource = Users;

            userView = userGrid.MainView as GridView;
            
            userView.OptionsView.ShowColumnHeaders = false;
            userView.OptionsView.ShowGroupPanel = false;
            userView.OptionsView.ShowGroupExpandCollapseButtons = false;
            userView.OptionsSelection.MultiSelect = false;
            
            userGrid.KeyDown += ListBoxControl1OnKeyDown;
            userGrid.Click += ListBoxControl1OnClick;

            userPopupContainer.Controls.Add(userGrid);
        }

        private void ListBoxControl1OnClick(object sender, EventArgs e)
        {
            SelectUser();
        }

        private void ListBoxControl1OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SelectUser();
                e.Handled = true;
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (!IsPopUpShown) 
                return;
            
            switch (e.KeyData)
            {
                case Keys.Down:
                case Keys.Up:
                    userGrid.Focus();
                    e.Handled = true;
                    break;
                case Keys.Enter:
                    SelectUser();
                    e.Handled = true;
                    break;
                case Keys.Right:
                case Keys.Left:
                    userPopupContainer.HidePopup();
                    break;
            }
        }

        private bool IsPopUpShown => userPopupContainer.Visible && userPopupContainer.Parent != null;

        public void Init(BarManager manager)
        {
            userPopupContainer.Manager = manager;
            _manager = manager;
        }

        private void textEditor_AutoCorrect(object sender, DevExpress.XtraRichEdit.AutoCorrectEventArgs e)
        {
            var caretPos = textEditor.Document.CaretPosition.ToInt();
            if (caretPos == 0)
            {
                e.AutoCorrectInfo = null;
                userPopupContainer.HidePopup();
                return;
            }

            if (string.IsNullOrWhiteSpace(e.AutoCorrectInfo.Text))
            {
                e.AutoCorrectInfo = null;
                userPopupContainer.HidePopup();
                return;
            }

            var searchResult = GetLastWord(caretPos);

            if (e.AutoCorrectInfo.Text == "@" || (searchResult.SymbolBeforeLastWord == "@" && !string.IsNullOrWhiteSpace(searchResult.LastWord)))
            {
                userView.FindFilterText = searchResult.LastWord;
                
                if (IsPopUpShown)
                {
                    e.AutoCorrectInfo = null;
                    return;
                }

                textEditor.BeginInvoke(new Action(() =>
                {
                    var position = textEditor.Document.CaretPosition;
                    var rect = textEditor.GetBoundsFromPosition(position);
                    var localRect =
                        DevExpress.Office.Utils.Units.DocumentsToPixels(rect, textEditor.DpiX, textEditor.DpiY);
                    var localPoint = new Point(localRect.Left, localRect.Bottom);
                    var absolutePoint = PointToScreen(localPoint);
                    userPopupContainer.ShowPopup(_manager, absolutePoint);
                    textEditor.Focus();

                    e.AutoCorrectInfo = null;
                }));
                return;
            }

            userPopupContainer.HidePopup();
            e.AutoCorrectInfo = null;
        }

        private LastWordSearchResult GetLastWord(int caretPos, string regexPattern = @"\w+")
        {
            var currentParagraph = textEditor.Document.Paragraphs.Get(textEditor.Document.CaretPosition);
            var searchRange = textEditor.Document.CreateRange(currentParagraph.Range.Start,
                caretPos - currentParagraph.Range.Start.ToInt());
            var searchResult =
                textEditor.Document.StartSearch(new Regex(regexPattern, RegexOptions.RightToLeft), searchRange);
            var lastWord = string.Empty;
            var symbolBeforeLastWord = string.Empty;

            if (searchResult.FindNext())
            {
                lastWord = textEditor.Document.GetText(searchResult.CurrentResult);
                var positionBefore = caretPos - lastWord.Length - 1;
                symbolBeforeLastWord = positionBefore >= 0
                    ? textEditor.Document.GetText(
                        textEditor.Document.CreateRange(caretPos - lastWord.Length - 1, 1))
                    : "";
            }

            return new LastWordSearchResult
            {
                LastWord = lastWord,
                SymbolBeforeLastWord = symbolBeforeLastWord,
                Range = searchResult.CurrentResult
            };
        }

        private void SelectUser()
        {
            var row = userView.GetSelectedRows()[0];
            var login = userView.GetRow(row) as string;
            
            var caretPos = textEditor.Document.CaretPosition.ToInt();
            if (caretPos == 0)
            {
                userPopupContainer.HidePopup();
                return;
            }

            var previousSymbolRange = textEditor.Document.CreateRange(caretPos - 1, 1);
            var previousSymbol = textEditor.Document.GetText(previousSymbolRange);
            if (previousSymbol == "@")
            {
                textEditor.Document.Replace(previousSymbolRange, $"@{login}");
                textEditor.Document.Hyperlinks.Create(previousSymbolRange);
                userPopupContainer.HidePopup();
                return;
            }

            var searchResult = GetLastWord(caretPos,@"@\w+");
            textEditor.Document.Replace(searchResult.Range, $"@{login}");
            textEditor.Document.Hyperlinks.Create(searchResult.Range);

            userPopupContainer.HidePopup();
        }
    }
}
